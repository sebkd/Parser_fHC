import json
import os
import sys

from pymongo import MongoClient

sys.path.insert(0, os.path.join(os.getcwd(), ''))


class JsonDB:

    @classmethod
    def create_json(cls, spider_name):
        with open('jsons/package_light.json', 'w', encoding='utf-8') as file_json:
            client = MongoClient('localhost', 27017)
            mongobase = client.fhc
            collection = mongobase[spider_name]
            dict_to_file = {}
            for index, item in enumerate(collection.find()):

                try:
                    dict_to_file.update({index:
                        {
                            "url": item['url'],
                            "catalog": item['catalog'],
                        }}
                    )
                except KeyError:
                    continue
                # if index > 500:
                #     break
            json.dump(obj=dict_to_file, fp=file_json, indent=4)


if __name__ == '__main__':
    # spider_name = 'stroyportal'
    spider_name = 'stroyportal_light'
    json_file = JsonDB
    json_file.create_json(spider_name=spider_name)
