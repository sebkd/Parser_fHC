import os
import sys

from scrapy.crawler import CrawlerRunner
from scrapy.settings import Settings
from scrapy.utils.log import configure_logging
from twisted.internet import reactor

sys.path.insert(0, os.path.join(os.getcwd(), ''))
import settings
from spiders.stroyportal import StroyportalSpider
from spiders.stroyportal_light import StroyportalLightSpider

if __name__ == '__main__':  # ctrl+j main

    configure_logging()

    crawler_settings = Settings()
    crawler_settings.setmodule(settings)

    runner = CrawlerRunner(settings=crawler_settings)

    # runner.crawl(StroyportalSpider)
    runner.crawl(StroyportalLightSpider)

    d = runner.join()
    d.addBoth(lambda _: reactor.stop())

    reactor.run()
