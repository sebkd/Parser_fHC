# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from itemloaders.processors import Compose


def clean_catalog(list_catalog):
    """Предварительная очистка списка каталога
    (убираем Каталог и название самого товара),
    если же есть ошибка, то возвращаем сам список -
    не теряем данные"""
    try:
        return list_catalog[1:-1]
    except:
        return list_catalog


def clean_catalog_light(list_catalog):
    """Предварительная очистка списка каталога
    (убираем Каталог),
    если же есть ошибка, то возвращаем сам список -
    не теряем данные"""
    try:
        return list_catalog[1:]
    except:
        return list_catalog


class FhcScraperItem(scrapy.Item):
    """Сборка вместе с товаром"""
    # define the fields for your item here like:
    name = scrapy.Field()
    url = scrapy.Field()
    cost = scrapy.Field()
    currency = scrapy.Field()
    unit = scrapy.Field()
    catalog = scrapy.Field(input_processor=Compose(clean_catalog))
    _id = scrapy.Field()


class FhcScraperLightItem(scrapy.Item):
    """Сборка только каталогов"""
    # define the fields for your item here like:
    url = scrapy.Field()
    catalog = scrapy.Field(input_processor=Compose(clean_catalog_light))
    _id = scrapy.Field()
