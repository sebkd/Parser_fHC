# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter

from pymongo import MongoClient


class FhcScraperPipeline:
    """Заносим данные в базу MongoDB, при случае парсинга с товаром
    и в случае парсинга только каталогов, так как здесь
    ничего не меняется"""

    def __init__(self):
        # не забывай включить процесс mongod.service
        # systemctl start mongod.service
        # systemctl status mongod.service
        client = MongoClient('localhost', 27017)
        self.mongobase = client.fhc

    def process_item(self, item, spider):
        collection = self.mongobase[spider.name]
        collection.insert_one(item)
        return item
