import os
import sys

import scrapy
from scrapy.http import HtmlResponse
from scrapy.loader import ItemLoader

sys.path.insert(0, os.path.join(os.getcwd(), 'fHC_scraper'))

from items import FhcScraperItem

MAINSECTION = 'mainsection'
USLUGI = '-uslugi-'
HTTPS = 'https://'


class StroyportalSpider(scrapy.Spider):
    """
    Класс пучка парсинга данных с необходимыми данными
    после работы этот модуль передает данные далее в моудль items
    """
    name = 'stroyportal'
    allowed_domains = ['stroyportal.ru']
    start_urls = ['https://www.stroyportal.ru/']


    def parse(self, response: HtmlResponse, **kwargs):
        """
        Функция первой страницы - с нее мы собираем главные каталоги и заходим в каждый
        :param response: путь первой страницы
        :param kwargs: параметры возможные страницы
        :return: yield-им следующую страницу в глубину
        """
        next_page_container = response.xpath("//div[@class='top-24']//@href").getall()
        # фильтруем группы товаров
        for page in next_page_container:
            # выборка главного каталога
            if MAINSECTION in page:
                # убираем не нужные
                if USLUGI not in page:
                    yield response.follow(
                        url=page,
                        callback=self.parse_second_page,
                        cb_kwargs={
                            'page': page,
                        },
                    )

    def parse_second_page(self, response: HtmlResponse, page):
        """Парсинг второй страницы (спускаемся ниже)"""
        next_page_container = response.xpath('//div[@class="row"]//a[contains(@href, "catalog")]/@href').getall()
        print()
        for next_page in next_page_container:
            yield response.follow(
                url=next_page,
                callback=self.parse_three_page,
                cb_kwargs={
                    'page': next_page,
                },
            )

    def parse_three_page(self, response: HtmlResponse, page):
        """Парсинг третьей страницы (еще один подкаталог)"""
        next_page_container = response.xpath('//div[@class="row"]//a[contains(@href, "catalog")]/@href').getall()
        for next_page in next_page_container:
            # убираем лишнии ссылки рекламы
            if HTTPS not in next_page:
                # # не критично, убираем лишние ссылки (не все, в одной подгруппе есть
                # # и совпадающие с основной группой ссылки)
                # if not re.search(PATTERN_RE, next_page):
                yield response.follow(
                    url=next_page,
                    callback=self.parse_final_page,
                    cb_kwargs={
                        'page': next_page,
                    },
                )

    def parse_final_page(self, response: HtmlResponse, page):
        """Смотрим финальную страничку, здесь буду раскрывать странички по 1,2,3 ... 889"""
        # проверка есть ли еще страницы для открытия
        is_next = response.xpath('//a[@id="NextLink"]/@href').get()
        if is_next:
            yield response.follow(
                url=is_next,
                callback=self.parse_final_page,
                cb_kwargs={
                    'page': is_next,
                },
            )
        # если страниц нет, то собираем товары со страницы и открываем страничку с товаром
        next_page_container = response.xpath('//span[@itemprop="name"]/../@href').getall()
        for next_page in next_page_container:
            yield response.follow(
                url=next_page,
                callback=self.parse_product_page,
                cb_kwargs={
                    'page': next_page,
                },
            )

    def parse_product_page(self, response: HtmlResponse, page):
        """Страница продукта, отсюда забираем все необходимые данные"""
        loader = ItemLoader(item=FhcScraperItem(), response=response)
        loader.add_xpath('name', '//h1[@itemprop="name"]/text()')
        loader.add_value('url', response.url)
        loader.add_xpath('cost', '//span[@itemprop="price"]/text()')
        loader.add_xpath('catalog', '//div[contains(@class, "breadcrumbs")]/a[*]/span[1]/text()')
        if self.checking(response.xpath('//span[@itemprop="price"]//..//span[last()]/text()').get()):
            loader.add_xpath('currency', '//span[@itemprop="price"]//..//span[last()-1]/text()')
            loader.add_xpath('unit', '//span[@itemprop="price"]//..//span[last()]/text()')
        else:
            loader.add_xpath('currency', '//span[@itemprop="price"]//..//span[last()-2]/text()')
            loader.add_xpath('unit', '//span[@itemprop="price"]//..//span[last()-1]/text()')

        yield loader.load_item()

    @staticmethod
    def checking(check_last_value):
        """Проверка последнего значения, от этого зависит парсинг правильный, он в динамике"""
        if 'наличии' not in check_last_value:
            return True
        else:
            return False
