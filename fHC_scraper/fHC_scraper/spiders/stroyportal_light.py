"""
Модуль парсинга только по каталогам
"""
import os
import sys

import scrapy
from scrapy.http import HtmlResponse
from scrapy.loader import ItemLoader

sys.path.insert(0, os.path.join(os.getcwd(), 'fHC_scraper'))

from items import FhcScraperLightItem

MAINSECTION = 'mainsection'
USLUGI = '-uslugi-'
HTTPS = 'https://'


class StroyportalLightSpider(scrapy.Spider):
    """
    Класс паука парсинга данных с необходимыми данными
    после работы этот модуль передает данные далее в моудль items
    """
    name = 'stroyportal_light'
    allowed_domains = ['stroyportal.ru']
    start_urls = ['https://www.stroyportal.ru/']

    def parse(self, response: HtmlResponse, **kwargs):
        """
        Функция первой страницы - с нее мы собираем главные каталоги и заходим в каждый
        :param response: путь первой страницы
        :param kwargs: параметры возможные страницы
        :return: yield-им следующую страницу в глубину
        """
        next_page_container = response.xpath("//div[@class='top-24']//@href").getall()
        # фильтруем группы товаров
        for page in next_page_container:
            # выборка главного каталога
            if MAINSECTION in page:
                # убираем не нужные
                if USLUGI not in page:
                    yield response.follow(
                        url=page,
                        callback=self.parse_second_page,
                        cb_kwargs={
                            'page': page,
                        },
                    )

    def parse_second_page(self, response: HtmlResponse, page):
        """Парсинг второй страницы (спускаемся ниже)"""
        next_page_container = response.xpath('//div[@class="row"]//a[contains(@href, "catalog")]/@href').getall()
        print()
        for next_page in next_page_container:
            yield response.follow(
                url=next_page,
                callback=self.parse_three_page,
                cb_kwargs={
                    'page': next_page,
                },
            )

    def parse_three_page(self, response: HtmlResponse, page):
        """Парсинг третьей страницы (еще один подкаталог)"""
        next_page_container = response.xpath('//div[@class="row"]//a[contains(@href, "catalog")]/@href').getall()
        for next_page in next_page_container:
            # убираем лишнии ссылки рекламы
            if HTTPS not in next_page:
                # # не критично, убираем лишние ссылки (не все, в одной подгруппе есть
                # # и совпадающие с основной группой ссылки)
                # if not re.search(PATTERN_RE, next_page):
                yield response.follow(
                    url=next_page,
                    callback=self.parse_final_page,
                    cb_kwargs={
                        'page': next_page,
                    },
                )

    def parse_final_page(self, response: HtmlResponse, page):
        """Смотрим финальную страничку и собираем каталоги"""
        loader = ItemLoader(item=FhcScraperLightItem(), response=response)
        loader.add_value('url', response.url)
        loader.add_xpath('catalog', '//div[contains(@class, "breadcrumbs")]/a[*]/span[1]/text()')
        yield loader.load_item()
